const { Router } = require('express');
const FighterService  = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();



router.get('/',  (req, res, next) => {
    try {
        
     return   FighterService.getAllFigters();
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
    try {
        const data = FighterService.search(req)
        return data
        // res.data = data;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.post('/',  (req, res, next) => {
    try {
        const data = FighterService.createFigter(req.body)
        return data
        // res.send(`the fighter ${req.body.name} created`);
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.put('/:id',  (req, res, next) => {
    try {
        const data = FighterService.updateFigter(req.body)
        return data
        // res.data = data;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
    try {
        const data = FighterService.delFigter(req)
        return data
        // res.data = data;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

// TODO: Implement route controllers for fighter

module.exports = router;