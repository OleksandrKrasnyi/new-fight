const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    getAllFigters()  

    createFighter(data) {
      const newFighter = FighterRepository.create(data)
      return newFighter
    }

    delFigter(data) {
      const fighter = FighterRepository.delete(data.id)
      return fighter
    }

    updateFigter(data) {
      const fighter = FighterRepository.update(data.id, data.dataToUpdate)
      return fighter
    }
    

    search(search) {
        const item = FighterRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
    // TODO: Implement methods to work with fighters
}

module.exports = new FighterService();