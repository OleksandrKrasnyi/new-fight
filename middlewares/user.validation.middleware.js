const { user } = require('../models/user');
const createUserValid = (req, res, next) => {
    const request = req.body
    if(!request.email.match(/[\w.\-]@gmail\.com[a-z]$/gi)) {
        res.send(`Wrong email format`)
    }
    if(!request.phoneNumber.match(/^\+380\d{9}$/)) {
        res.send(`Wrong email format`)
    }
    // TODO: Implement validatior for user entity during creation

    next();
}

const updateUserValid = (req, res, next) => {
    const request = req.body
    if(!request.email.match(/[\w.\-]@gmail\.com[a-z]$/gi)) {
        res.send(`Wrong email format`)
    }
    if(!request.phoneNumber.match(/^\+380\d{9}$/)) {
        res.send(`Wrong email format`)
    }
    // TODO: Implement validatior for user entity during update

    next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;